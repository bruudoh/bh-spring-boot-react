terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

# Let reference the EC2 module
module "webserver" {
  source = "./modules/ec2"

  servername = "spring-demo"
  size       = "t2.micro"

}
